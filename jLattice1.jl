#!/usr/bin/env julia-latest
# -*- mode: julia; -*- 
# ~ jiw ~  2023 ~  ~

# Re: Drawing non-crossing "pencil line" traces on a lattice as ref'd
# in files # 26147, 26148, 26149, 26150, 26151, 26153, 26154, 26156,
# 26157, 26159, 26161, 26163, 26165, 26167, 26169, 26170, 26172,
# 26178, 26181, 26183, 26185, 26187, 26189, 26192, 26194 in ./msg

# jLattice draws a spiral out from (0,0) to a specified size of spiral
# and then back in, in a single-trace no-lift no-cross manner.

module jLattice
using Luxor
const deltas = Dict('U'=>Point(0,-1), 'D'=>Point(0,1),
                    'L'=>Point(-1,0), 'R'=>Point(1,0))
function  main()
    # Get params: max side steps, n;  scale;  line width; corner fraction
    arn = 0;    nargs = length(ARGS)
    arn+=1; n = nargs>=arn ? parse(Int64, ARGS[arn]) : 5
    arn+=1; scale = nargs>=arn ? parse(Int64, ARGS[arn]) : 71
    arn+=1; lwide = nargs>=arn ? parse(Float64, ARGS[arn]) : abs(scale/17)
    arn+=1; cornerF = nargs>=arn ? parse(Float64, ARGS[arn]) : 0.15
    arn+=1; shoPar  = nargs>=arn ? parse(Int64, ARGS[arn]) : 0
    if n < 2 || lwide < 0.01
        println("n=$n or w=$lwide out of range"); exit()
    end
    # cornerF is fraction of edge drawn as 45° corner line
    cornerE = 1-cornerF
    dsize = 1000
    NN = n < 10 ? "0$n" : "$n"
    Drawing(dsize, dsize, "image-$NN.png")
    background("grey7");    setline(lwide)
    origin()                    # Reset origin to center of drawing
    p1 = p2 = Point(0,0)        # Luxor points p1, p2 start at origin

    function drawString(s)      # Draw U/D/L/R steps as listed in s
        for code in s
            # Multiply (dx, dy) pair from deltas[] by scale factor
            p1, p2 = p1 + scale*deltas[code], p1
            line(cornerE*p2 + cornerF*p1) # draw 45° corner line
            line(cornerE*p1 + cornerF*p2) # draw H or V line part
        end
    end

    function drawTwo(ss)
        # Draw angle-steps on 2 sides of square, going CCW for green,
        # CW for blue, making `steps` right angles per side, except
        # `steps-1/2` right angles for the last CCW side.  Note,
        # caller sets ss[1] and ss[2] to handle 4 cases of chirality
        # and #steps: ("DR","UR") for CCW, even; ("UL","DL") for CCW,
        # odd; ("RU","RD") for CW, even; and ("LD","LU") for CW, odd.
        drawString(ss[1]^steps)
        drawString(ss[2]^(steps-1))
        # Half-elbow going-out to going-in path turnaround
        drawString(steps < n ? ss[2] : ss[2][1])
    end

    sethue("green")
    line(p1, p1)                # Begin the path
    drawString("R")         # Initial five links at origin
    # Draw the spiralling-out green trace
    steps = 1
    while steps ≤ n         # DR & UR for even, UL & DL for odd
        drawTwo(iseven(steps) ? ("DR","UR") : ("UL","DL"))
        steps += 1
    end
    pat = cornerE*p1 + cornerF*p2
    line(pat, pat, :stroke)          # Stroke green path
    # Draw the spiralling-in part of trace in blue
    sethue("blue");   line(pat, pat)  # Begin blue path
    steps -= 2
    while steps ≥ 1         # RU & RD for even, LD & LU for odd
        drawTwo(iseven(steps) ? ("RU","RD") : ("LD","LU"))
        steps -= 1
    end
    line(p1, p1, :stroke)       # Stroke blue path
    if shoPar > 0
        sethue("white")
        fontsize(24)
        fontface("Georgia Bold")
        text("n=$n  s=$scale  w=$lwide  f=$cornerF",
             Point(dsize/2,-dsize/2), halign=:right, valign = :top)
    end
    finish()                    # Write plot to output .png
end
main()
end # module
