.. -*- mode: rst -*-
..  To view this as a local file in browser, use `restview README.rst`
..  Browser page will update whenever a changed version is stored.

===============
mxnSquares
===============

Project Contents:
------------------

• `jLattice1.jl` - Program that draws a "square" in a single-trace
  no-lift no-cross manner on the integer XY lattice.  See .png's for
  example outputs.
• `image-N.png` (where N represents an integer) - Example outputs for
  various values of `n`  

Notes:
------------------

`jLattice1.jl`, written by James Waldby on 8 June 2023, is a
Julia-language program that can be run by `./jLattice1.jl` on typical
linux systems, or by `julia jLattice1.jl` otherwise.  When run, it
produces a .PNG picture of a trace on part of the integer XY lattice.
(This lattice is all points on the XY plane that have integer
coordinates.)

A no-lift no-cross trace on the lattice, as drawn by the program, is a
connected sequence of unit steps drawn point to point without lifting
the pen or pencil or crossing the line already drawn.

Each step in the trace is one unit long, from a point to an adjacent
point.  The trace that `jLattice1.jl` draws starts at the origin
(0,0), spirals out to about some distance n, then spirals back in,
resulting in a basically square area (tilted 45°) with all links drawn
in.  This should be the path that Dan Joyce described in posts in the
sci.math newsgroup, during early June 2023, with subjects like "Draw
an infinite square lattice without lifting the pencil".

`jLattice1.jl` produces a file with a name of form `image-N.png`
(where N represents an integer).  When run with no command-line
parameters specified, it produces the same output (in file
`image-05.png`) as would `./jLattice1.jl 5 71 4.2 0.15 0`.  See ¶
Parameter Details for explanation of the numbers.


Program Overview:
------------------

At start of main, get values of n, scale, linewidth, and corner
fraction parameters unless they are defaulted. (See *Parameter
Details* below).  A Drawing(...) call starts a Luxor plot page.  Next,
main defines two internal functions, drawString(s) and drawTwo(ss),
described below.  After function definitions and some plot setup, main
draws the very first R step at center of square, after which two loops
draw the outgoing trace and the ingoing trace, as follows.

*Outgoing*: main has a loop that calls drawTwo to draw sides with 1,
2, 3, 4 ... n right angles per side going CCW.  Specifically, it uses
1 UL, 1 DL, 2 DR, 2 UR, 3 UL, 3 DL, 4 DR, 4 UR, 5 UL, 5 DL, etc up to
n and n-1/2 steps for the last two sides.

The trace turns around at a right-hand corner if n is even, or at a
left-hand corner if n is odd.  The coordinates of the point where
turnaround and green-to-blue change occurs are (n,0) for even n, and
(1-n,0) for odd n.

*Ingoing*: A loop calls drawTwo for sides with k = n-1, n-2,..., 3, 2,
1 right angles per side going CW.  When k is even it draws k RU and k
RD steps.  When k is odd it draws k LD and k LU steps.

Finally there's optional display of parameter values at top right of
picture, and finish() to close out the plot and write a .png file.

*Function drawString(s)*: This does all U,D,L,R moves listed in
string s. Points p1 and p2 are next and current corners.

*Function drawTwo(ss)*: This draws two side-runs of square as noted in
its comments, drawing only half of the last right angle in last CCW
side.

Parameter Details:
------------------

The program accepts parameters for `n` (overall size), `scale`
(segment length), `lwide` (line width), and `cornerF` (edge drawing
fraction), and `shoPar` (show params on image). 

In slightly more detail: `n` is how many excursions there are on the
longest sides of generated square; `scale` is the dot-to-dot segment
length, in pixels; `lwide` is line width, also in pixels; `cornerF` is
fraction of edge to be drawn as a 45° corner line; and `shoPar` is a
0/1 flag for whether to display parameter values at top right in the
picture output.

If `n<2` or if `lwide<0.01`, the program will exit without drawing.
If `scale<0`, image will be reflected left to right and top to bottom.

Small values of `lwide` (eg 0.1 or less) give a trace that's barely
visible.  Large values (eg comparable to `scale`) produce blobby
pictures.

If `cornerF` is nonzero, `jLattice1.jl` cuts across (at 45°) near each
corner, rather than drawing every corner as a square, right angle
turn.  By default, it cuts across when closer than 0.15 units from a
corner.  Values of `cornerF` in range 0 to 0.5 control corner-cutting,
using between 0.0 and 0.5 of each unit step as a corner cut.  Values
slightly outside the 0-0.5 range (eg, -0.1, 0.6) produce slight
overshoots at corners - short lines at 0, 45, and 90° that project
beyond corners - while values far outside the 0-0.5 range (eg 20)
produce long overshoot lines at the same angles.  Images #08 and #09
illustrate results with `cornerF` set to 0.5 and 0, respectively.

If `shoPar` is nonzero, values of the first 4 parameters will appear as
text in the top right corner of the output picture.



